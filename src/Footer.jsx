import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <div className="bg-slate-800 border-t-2 border-slate-600 p-6 flex justify-between flex-wrap">
        <div className="flex gap-5">
          <Link to="/">
            <p className="text-stone-200 hover:text-green-300 ease-out duration-300 font-bold">
              About
            </p>
          </Link>

          <Link to="/articles">
            <p className="text-stone-200 hover:text-green-300 ease-out duration-300 font-bold">
              Articles
            </p>
          </Link>
          <Link to="/project">
            <p className="text-stone-200 hover:text-green-300 ease-out duration-300 font-bold">
              Project
            </p>
          </Link>
          <Link to="/speaking">
            <p className="text-stone-200 hover:text-green-300 ease-out duration-300 font-bold">
              Speaking
            </p>
          </Link>
          <Link to="/uses">
            <p className="text-stone-200 hover:text-green-400 ease-out duration-300 font-bold">
              Uses
            </p>
          </Link>
        </div>
        <p className="text-gray-500">
          © 2023 Spencer Sharp. All rights reserved.
        </p>
      </div>
    </>
  );
};

export default Footer;
