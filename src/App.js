import React from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Applayout from "./Applayout";
import About from "./pages/About";
import Articles from "./pages/Articles";
import Project from "./pages/Project";
import Speaking from "./pages/Speaking";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Applayout />,
      children: [
        {
          path: "/",
          element: <About />,
        },
        {
          path: "/articles",
          element: <Articles />,
        },
        {
          path: "/project",
          element: <Project />,
        },
        {
          path: "/speaking",
          element: <Speaking />,
        },
      ],
    },
  ]);
  return <RouterProvider router={router} />;
}

export default App;
