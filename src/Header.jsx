import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div className="gap-6 flex bg-slate-700 p-3 px-6 justify-around mx-auto rounded-3xl">
      <Link to="/">
        <p className="text-stone-200 hover:text-green-300 ease-out duration-300 hover:tracking-widest">
          About
        </p>
      </Link>
      <Link to="/articles">
        <p className="text-stone-200 hover:text-green-300 ease-out duration-300 hover:tracking-widest">
          Articles
        </p>
      </Link>
      <Link to="/project">
        <p className="text-stone-200 hover:text-green-300 ease-out duration-300 hover:tracking-widest">
          Project
        </p>
      </Link>
      <Link to="/speaking">
        <p className="text-stone-200 hover:text-green-300 ease-out duration-300 hover:tracking-widest">
          Speaking
        </p>
      </Link>
    </div>
  );
};

export default Header;
