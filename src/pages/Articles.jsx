import React from "react";

const Articles = () => {
  return (
    <>
      <div className="pt-12">
        <p className="text-white text-4xl md:text-6xl">
          Writing on software <br /> design, company building, <br /> and the
          aerospace industry.
        </p>
        <p className="text-gray-400  mt-[50px] text-lg">
          All of my long-form thoughts on programming, leadership, product
          design, and more,
          <br /> collected in chronological order.
        </p>
        <div className="flex border-gray-400  gap-12 mt-5">
          <div>
            <p className="text-gray-400  mt-[50px]">September 5, 2022</p>
          </div>
          <div className=" hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
            <p className="text-white text-lg pt-[47px]">
              Crafting a design system for multiplanetary future
            </p>
            <p className="text-gray-400 text-sm w-[400px] pt-5">
              Writing on software design, company building, and the aerospace
              industry. All of my long-form thoughts on programming, leadership,
              product design, and more, collected in chronological order.
            </p>
            <p className="text-sm text-green-300 pt-5 cursor-pointer">
              Read article{" "}
            </p>
          </div>
        </div>
        <div className="flex border-gray-400 border-solid gap-12 mt-5">
          <div>
            <p className="text-gray-400  mt-[50px]">September 5, 2022</p>
          </div>
          <div className=" hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
            <p className="text-white text-lg pt-[47px]">
              Crafting a design system for multiplanetary future
            </p>
            <p className="text-gray-400 text-sm w-[400px] pt-5">
              Writing on software design, company building, and the aerospace
              industry. All of my long-form thoughts on programming, leadership,
              product design, and more, collected in chronological order.
            </p>
            <p className="text-sm text-green-300 pt-5 cursor-pointer">
              Read article{" "}
            </p>
          </div>
        </div>
        <div className="flex border-gray-400 border-solid gap-12 mt-5">
          <div>
            <p className="text-gray-400   mt-[50px]">September 5, 2022</p>
          </div>
          <div className=" hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
            <p className="text-white text-lg pt-[47px]">
              Crafting a design system for multiplanetary future
            </p>
            <p className="text-gray-400 text-sm w-[400px] pt-5">
              Writing on software design, company building, and the aerospace
              industry. All of my long-form thoughts on programming, leadership,
              product design, and more, collected in chronological order.
            </p>
            <p className="text-sm text-green-300 pt-5 cursor-pointer">
              Read article{" "}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Articles;
