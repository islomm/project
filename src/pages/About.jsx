import React from "react";

const About = () => {
  return (
    <div className="h-fit pt-12">
      <div className="flex gap-[150px] flex-wrap">
        <div>
          <p className="text-white text-5xl ">
            I’m Spencer Sharp. I <br /> live in New York City, <br /> where I
            design the <br />
            future.
          </p>
          <p className="text-gray-400 w-[450px] mt-[50px] ">
            I’ve loved making things for as long as I can remember, and wrote my
            first program when I was 6 years old, just two weeks after my mom
            brought home the brand new Macintosh LC 550 that I taught myself to
            type on.
            <br></br>
            <br />
            The only thing I loved more than computers as a kid was space. When
            I was 8, I climbed the 40-foot oak tree at the back of our yard
            while wearing my older sister’s motorcycle helmet, counted down from
            three, and jumped — hoping the tree was tall enough that with just a
            bit of momentum I’d be able to get to orbit.
            <br></br>
            <br />I spent the next few summers indoors working on a rocket
            design, while I recovered from the multiple surgeries it took to fix
            my badly broken legs. It took nine iterations, but when I was 15 I
            sent my dad’s Blackberry into orbit and was able to transmit a photo
            back down to our family computer from space.
          </p>
        </div>
        <div>
          <img
            width={450}
            height={450}
            className="rounded-3xl rotate-[6deg] lg:mt-12"
            src="https://spotlight.tailwindui.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fportrait.79754e9e.jpg&w=640&q=75"
          />
        </div>
      </div>
    </div>
  );
};

export default About;
