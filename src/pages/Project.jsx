import React from "react";

const Project = () => {
  return (
    <>
      <div className="pt-12">
        <p className="text-white text-6xl font-bold">
          Things I’ve made trying to put <br /> my dent in the universe.
        </p>
        <p className="text-gray-400 pt-7">
          I’ve worked on tons of little projects over the years but these are
          the ones that I’m most proud <br /> of. Many of them are open-source,
          so if you see something that piques your interest, check out <br />{" "}
          the code and contribute if you have ideas for how it can be improved.
        </p>
      </div>
      <div className="flex flex-wrap pt-12 gap-7">
        <div className=" w-[310px]  hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
          <img
            width={60}
            height={60}
            className="rounded-full border-white border-[3px] p-1"
            src="https://avatars.mds.yandex.net/i?id=680852d20084ef07bf28af4751dea489cfe7cafb-10285207-images-thumbs&n=13"
            alt=""
          />
          <p className="text-white text-lg pt-5">Planetaria</p>
          <p className="text-gray-400 pt-5">
            Create technology to empower civilians to <br /> explore space on
            ther
          </p>
          <p className="font-bold text-white pt-5 hover:text-green-300 cursor-pointer">
            Planetaria tech
          </p>
        </div>
        <div className="w-[310px] hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
          <img
            width={60}
            height={60}
            className="rounded-full border-white border-[3px] p-1"
            src="https://avatars.mds.yandex.net/i?id=1f378f65d53fbebf9c361572cf590851-5888781-images-thumbs&n=13"
            alt=""
          />
          <p className="text-white text-lg pt-5">Antimaginary</p>
          <p className="text-gray-400 pt-5">
            Create technology to empower civilians to <br /> explore space on
            ther own terms long-form
          </p>
          <p className="font-bold text-white pt-5 hover:text-green-300 cursor-pointer">
            github.com
          </p>
        </div>
        <div className="w-[310px] hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300">
          <img
            width={60}
            height={60}
            className="rounded-full border-white border-[3px] p-1"
            src="https://avatars.mds.yandex.net/i?id=3f75ea300ab68c5f4bb0b4994540199e-4405807-images-thumbs&n=13"
            alt=""
          />
          <p className="text-white text-lg pt-5">HelioStream</p>
          <p className="text-gray-400 pt-5">
            Create technology to empower civilians to <br /> explore space on
            ther own terms
          </p>
          <p className="font-bold text-white pt-5 hover:text-green-300 cursor-pointer">
            github.com
          </p>
        </div>
        <div className="w-[310px] hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300 mt-5">
          <img
            width={60}
            height={60}
            className="rounded-full border-white border-[3px] p-1"
            src="https://avatars.mds.yandex.net/i?id=04c76b52ff8cb773560fa7c2075be0d1355b235f-10160381-images-thumbs&n=13"
            alt=""
          />
          <p className="text-white text-lg pt-5">CosmoOS</p>
          <p className="text-gray-400 pt-5">
            Create technology to empower civilians to <br /> explore space on
            ther own terms
          </p>
          <p className="font-bold text-white pt-5 hover:text-green-300 cursor-pointer">
            github.com
          </p>
        </div>
        <div className="w-[310px] hover:bg-slate-600 rounded-2xl p-5 ease-out duration-300 mt-5">
          <img
            width={60}
            height={60}
            className="rounded-full border-white border-[3px] p-1"
            src="https://avatars.mds.yandex.net/i?id=104df8c021e13c3f5aff5df2ba6d2b61b006a11e-10340155-images-thumbs&n=13"
            alt=""
          />
          <p className="text-white text-lg pt-5">OpenShuttle</p>
          <p className="text-gray-400 pt-5">
            Create technology to empower civilians to <br /> explore space on
            ther own terms
          </p>
          <p className="font-bold text-white pt-5 hover:text-green-300 cursor-pointer">
            github.com
          </p>
        </div>
      </div>
    </>
  );
};

export default Project;
