import React, { useEffect, useState } from "react";
import Header from "./Header";
import { Link, Outlet } from "react-router-dom";
import Footer from "./Footer";

const Applayout = () => {
  return (
    <div className="bg-black">
      <div className="max-w-screen-xl mx-auto bg-slate-800  pt-5 px-[100px] font-Robo border-2 border-gray-600">
        <div className="flex justify-around">
          <Link to="/">
            <img
              className="rounded-full border-[2px] border-gray-500 p-1 cursor-pointer"
              height={55}
              width={55}
              src="https://avatars.mds.yandex.net/i?id=49de0d11a17d7850f6cf600612e9950c9b146dbe-10879920-images-thumbs&n=13"
              alt=""
            />
          </Link>
          <Header />
          <img
            className="rounded-full border-[2px] border-gray-500 p-1 cursor-pointer"
            height={55}
            width={55}
            src="https://avatars.mds.yandex.net/i?id=d1fa347e4beaddb5ce3438f1f8bb71afe8e4148b-10638736-images-thumbs&n=13"
            alt=""
          />
        </div>

        <div className="pt-10">
          <Outlet />
        </div>
        <div className="pt-12">
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Applayout;
