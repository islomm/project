/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        Robo: "Open sans , sans , sans-serif",
      },
      rotate: {
        little: "20",
      },
    },
  },
  plugins: [],
};
